package com.service;



import java.util.List;

import com.model.Employee;

public interface ServentService {
	
	public void createServent(Employee servent);
	
	public List<Employee> getAllServents();
	
	public void deleteEmp(int sno);
	
	public Employee getByName(String name);
	
	
	
	public String updatedSalary(double salary, String name);
	
	public String updatedAdv(double adv, String name);

}
