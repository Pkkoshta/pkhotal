package com.service;



import com.dao.ServentDao;
import com.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ServentsServiceImpl implements ServentService {
	@Autowired
	private ServentDao dao;


	@Override
	public  void createServent(Employee servent) {
		Employee employee = new Employee();
		double bal = servent.getSalary() - servent.getAdvence();
		employee.setName(servent.getName());
		employee.setLastName(servent.getLastName());
		employee.setAdress(servent.getAdress());
		employee.setAdvence(servent.getAdvence());
		employee.setSalary(servent.getSalary());
		employee.setMobileNumber(servent.getMobileNumber());
		employee.setBalance(bal);
		employee.setDateOJ(servent.getDateOJ());
		employee.setDateOfRegine(servent.getDateOfRegine());
		dao.save(employee);
	}


	@Override
	public List<Employee> getAllServents() {
		return dao.findAll();
	}


	@Override
	public Employee getByName(String name) {		
		return dao.getServentsEntityByName(name);
	}


	@Override
	public String updatedSalary( double salary, String name) {
		double count =  dao.updateSal(salary, name);
		if(count != 0)
			return  name+" Salary Updated" + salary;
		else
			return "Not updated..";
	}


	@Override
	public String updatedAdv(double adv, String name) {
	    int count = 0;
	    count = dao.updateAdv(adv, name);
	    if(count != 0 )
		return "Adavence amount updated.... "+adv ;
	    else 
	    	return "Advence amount is not updated...!";
	}


	@Override
	public void deleteEmp(int sno) {
		dao.deleteById(sno);		
	}

}
