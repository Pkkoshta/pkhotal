package com.service;

import com.dao.HotalDao;
import com.model.HotalMGMT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class HotalServiceImpl implements HotalService {	
	@Autowired
	private HotalDao dao;

	@Override
	public List<HotalMGMT> getAllRecords() {
		List<HotalMGMT> listRec = dao.findAll();
		return listRec;
	}

	@Override
	public HotalMGMT saveReco(HotalMGMT hotal) {
		HotalMGMT hotmgmt = new HotalMGMT();
		double bal = 0 ;
		bal = hotal.getEarn() - hotal.getExpences();
		hotmgmt.setBal(bal);
		hotmgmt.setDate(hotal.getDate());
		hotmgmt.setEarn(hotal.getEarn());
		hotmgmt.setExpences(hotal.getExpences());		
		return dao.save(hotmgmt);
	}
	@Override
	public Long getTotalRec() {
		return dao.count();
	}

	@Override
	public void  delete(int sno) {	
		dao.deleteById(sno);
	}

	@Override
	public double getTotalBal() {
		List<HotalMGMT> hotal = dao.findAll();
		double bal = 0.0;
		for(HotalMGMT h:hotal) {
			bal = bal + h.getBal();
		}
		return bal;
	}

}
