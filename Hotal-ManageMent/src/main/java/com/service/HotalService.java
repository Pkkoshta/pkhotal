package com.service;

import java.util.List;

import com.model.HotalMGMT;

public interface HotalService {
	
	public Long getTotalRec() ;
	
	public double getTotalBal();
	
	public List<HotalMGMT> getAllRecords();
	
	public HotalMGMT saveReco(HotalMGMT hotal);
	
	public void delete(int sno);

}
