package com.controller;

import com.model.Employee;
import com.model.HotalMGMT;
import com.model.PaytmDetails;
import com.paytm.pg.merchant.CheckSumServiceHelper;
import com.service.HotalService;
import com.service.ServentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;



@CrossOrigin(origins = "http://localhost:5000")
@RestController
@RequestMapping("/api")
public class HotalController {
	
	@Autowired
	private PaytmDetails paytmDetails;

	@Autowired
	private Environment evn;

	@Autowired
	private HotalService service;
	
	@Autowired
	private ServentService serviceEmp;

	@GetMapping("/pay")
	public String home() {
		return "home";
	}

	@PostMapping(value = "/pgredirect")
	public ModelAndView getRedirect(@RequestParam(name = "CUST_ID") String customerId,
									@RequestParam(name = "TXN_AMOUNT") String transactionAmount,
									@RequestParam(name = "ORDER_ID") String orderId) throws Exception {

		ModelAndView modelAndView = new ModelAndView("redirect:" + paytmDetails.getPaytmUrl());
		TreeMap<String, String> parameters = new TreeMap<>();
		paytmDetails.getDetails().forEach((k, v) -> parameters.put(k, v));
		parameters.put("MOBILE_NO", evn.getProperty("paytm.mobile"));
		parameters.put("EMAIL", evn.getProperty("paytm.email"));
		parameters.put("ORDER_ID", orderId);
		parameters.put("TXN_AMOUNT", transactionAmount);
		parameters.put("CUST_ID", customerId);
		String checkSum = getCheckSum(parameters);
		parameters.put("CHECKSUMHASH", checkSum);
		modelAndView.addAllObjects(parameters);
		return modelAndView;
	}


	@PostMapping(value = "/pgresponse")
	public String getResponseRedirect(HttpServletRequest request, Model model) {

		Map<String, String[]> mapData = request.getParameterMap();
		TreeMap<String, String> parameters = new TreeMap<String, String>();
		mapData.forEach((key, val) -> parameters.put(key, val[0]));
		String paytmChecksum = "";
		if (mapData.containsKey("CHECKSUMHASH")) {
			paytmChecksum = mapData.get("CHECKSUMHASH")[0];
		}
		String result;

		boolean isValideChecksum = false;
		System.out.println("RESULT : "+parameters.toString());
		try {
			isValideChecksum = validateCheckSum(parameters, paytmChecksum);
			if (isValideChecksum && parameters.containsKey("RESPCODE")) {
				if (parameters.get("RESPCODE").equals("01")) {
					result = "Payment Successful";
				} else {
					result = "Payment Failed";
				}
			} else {
				result = "Checksum mismatched";
			}
		} catch (Exception e) {
			result = e.toString();
		}
		model.addAttribute("result",result);
		parameters.remove("CHECKSUMHASH");
		model.addAttribute("parameters",parameters);
		return "report";
	}

	private boolean validateCheckSum(TreeMap<String, String> parameters, String paytmChecksum) throws Exception {
		return CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(paytmDetails.getMerchantKey(),
				parameters, paytmChecksum);
	}


	private String getCheckSum(TreeMap<String, String> parameters) throws Exception {
		return CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(paytmDetails.getMerchantKey(), parameters);
	}





	//@CrossOrigin(origins = "http://localhost:4201")
    @DeleteMapping("/emp/{sno}")
	public List<Employee> deleteBySno(@PathVariable("sno") int sno){
		serviceEmp.deleteEmp(sno);
		return serviceEmp.getAllServents();
	}


	@PostMapping("/saveEmp")
	public List<Employee> create(@RequestBody Employee entity){
		 serviceEmp.createServent(entity);
		 return serviceEmp.getAllServents();
	}
	//@CrossOrigin(origins = "http://localhost:4201")
	@GetMapping("/all")
	public ResponseEntity<List<Employee>> getAllServents(){
		List<Employee> emp = serviceEmp.getAllServents();
		return new  ResponseEntity<List<Employee>>(emp,
				HttpStatus.OK);
	}


	//@CrossOrigin(origins = "http://localhost:4201")
	@GetMapping("/{name}")
	public Employee getByNameServents(@PathVariable("name") String name){
		return serviceEmp.getByName(name);
	}

	@GetMapping("/updated/salary/{sal}/{name}")
public String updatedSal(@PathVariable("sal") double salary, @PathVariable("name") String name) {
		return serviceEmp.updatedSalary(salary, name);

	}
	@GetMapping("update/advence/{adv}/{name}")
	public String updatedAdavence(@PathVariable("adv") double advence, @PathVariable("name") String name) {
		return serviceEmp.updatedAdv(advence, name);

	}
	/*Hotal Owner */
	//@CrossOrigin(value = "http://localhost:4201")
	@GetMapping("allRecords/")
	public List<HotalMGMT> getAllReco(){
		return service.getAllRecords();

	}

	//@CrossOrigin(value = "http://localhost:4201")
	@PostMapping("save/")
	public List<HotalMGMT> createRec(@RequestBody HotalMGMT hotal) {
	 service.saveReco(hotal);
	 return service.getAllRecords();
	}
	//@CrossOrigin(value = "http://localhost:4201")
	@DeleteMapping("/{sno}")
	public List<HotalMGMT> deleteReco(@PathVariable( "sno")int sno) {
		 service.delete(sno);
		 return service.getAllRecords();
	}
	//@CrossOrigin(value = "localhost:/4201")
	@GetMapping("totalBal/")
	public double getTotalBal() {
		return service.getTotalBal();
	}
	@GetMapping("totalRec/")
	public Long getTotalDay() {
		return service.getTotalRec();
	}
}
