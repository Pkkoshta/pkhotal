package com.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.model.Employee;

public interface ServentDao  extends JpaRepository<Employee, Integer>{

	@Query("SELECT e FROM Employee e WHERE e.name = :name")
	public Employee getServentsEntityByName( @Param("name") String name);
	
	@Transactional
	@Modifying
	@Query("UPDATE Employee e SET e.salary = e.salary + :salary  WHERE e.name = :name")
	public int updateSal(@Param("salary")double sal, @Param("name") String name);
	
	@Transactional
	@Modifying
	@Query("UPDATE Employee e SET e.advence = e.advence + :adv  WHERE e.name = :name")
	public int updateAdv(@Param("adv")double adv, @Param("name") String name);

	
}
