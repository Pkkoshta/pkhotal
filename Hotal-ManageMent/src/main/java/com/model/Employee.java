package com.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer sno;
	private String name;
	private String lastName;
	private String adress;
	private Long mobileNumber;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateOJ= new java.sql.Date(new java.util.Date().getTime());
	//@Temporal(TemporalType.TIMESTAMP)
	private Date dateOfRegine = new java.sql.Date(new java.util.Date().getTime());
	private double salary;
	private double advence;
	private double balance;
	
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public Long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Date getDateOJ() {
		return dateOJ;
	}
	public void setDateOJ(Date dateOJ) {
		this.dateOJ = dateOJ;
	}
	public Date getDateOfRegine() {
		return dateOfRegine;
	}
	public void setDateOfRegine(Date dateOfRegine) {
		this.dateOfRegine = dateOfRegine;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public double getAdvence() {
		return advence;
	}
	public void setAdvence(double advence) {
		this.advence = advence;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "ServentsEntity [sno=" + sno + ", name=" + name + ", lastName=" + lastName + ", adress=" + adress
				+ ", mobileNumber=" + mobileNumber + ", dateOJ=" + dateOJ + ", dateOfRegine=" + dateOfRegine
				+ ", salary=" + salary + ", advence=" + advence + ", balance=" + balance + "]";
	}
	
	

}
