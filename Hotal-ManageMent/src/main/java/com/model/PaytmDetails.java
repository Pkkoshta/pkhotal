package com.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties("paytm.payment.sandbox")
public class PaytmDetails {

    private String merchantId;

    private String merchantKey;

    private String channelId;

    private  String website;

    private  String industryTypeId;

    private String paytmUrl;

    private Map<String, String> details;

    public PaytmDetails() {
    }

    public PaytmDetails(String merchantId, String merchantKey, String channelId, String website, String industryTypeId, String paytmUrl, Map<String, String> details) {
        this.merchantId = merchantId;
        this.merchantKey = merchantKey;
        this.channelId = channelId;
        this.website = website;
        this.industryTypeId = industryTypeId;
        this.paytmUrl = paytmUrl;
        this.details = details;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getWebsite() {
        return website;
    }

    public String getIndustryTypeId() {
        return industryTypeId;
    }

    public String getPaytmUrl() {
        return paytmUrl;
    }

    public Map<String, String> getDetails() {
        return details;
    }

    @Override
    public String toString() {
        return "PaytmDetails{" +
                "merchantId='" + merchantId + '\'' +
                ", merchantKey='" + merchantKey + '\'' +
                ", channelId='" + channelId + '\'' +
                ", website='" + website + '\'' +
                ", industryTypeId='" + industryTypeId + '\'' +
                ", paytmUrl='" + paytmUrl + '\'' +
                ", details=" + details +
                '}';
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setIndustryTypeId(String industryTypeId) {
        this.industryTypeId = industryTypeId;
    }

    public void setPaytmUrl(String paytmUrl) {
        this.paytmUrl = paytmUrl;
    }

    public void setDetails(Map<String, String> details) {
        this.details = details;
    }
}
