package com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@javax.persistence.Table
public class HotalMGMT {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer sno;
	@Temporal(TemporalType.TIMESTAMP)
	private Date date = new java.sql.Date(new java.util.Date().getTime());
	private double expences;
	private double earn;
	private double bal;
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getExpences() {
		return expences;
	}
	public void setExpences(double expences) {
		this.expences = expences;
	}
	public double getEarn() {
		return earn;
	}
	public void setEarn(double earn) {
		this.earn = earn;
	}
	public double getBal() {
		return bal;
	}
	public void setBal(double bal) {
		this.bal = bal;
	}
	@Override
	public String toString() {
		return "HotalMGMT [sno=" + sno + ", date=" + date + ", expences=" + expences + ", earn=" + earn + ", bal=" + bal
				+ "]";
	}



}
